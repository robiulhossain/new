﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_Attachment")]
    public class Attachment 
    {
        [Key]
        public int Id { get; set; }
        public int IssueId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Type { get; set; }             
    }
}
