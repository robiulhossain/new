﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_Issue")]
    public class Issue : Entity
    {  
        [Key]
        public int Id { get; set; }
        public int? ProjectId { get; set; }
        public int? ClientId { get; set; }
        public int? AssigneeId { get; set; }
        public int? ReporterId { get; set; }
        public int IssueTypeId { get; set; }
        public int? SprintId { get; set; }
        public int StatusId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }    
        public int? ParentId { get; set; }
        public DateTime DueDate { get; set; }
    }
}
