﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_Project")]
    public class Project : Entity
    {
        [Key]
        public int Id { get; set; }
        public int? ClientId { get; set; }
        public string ProjectName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }             
    }
}
