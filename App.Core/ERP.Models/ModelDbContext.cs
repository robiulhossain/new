﻿using ERP.Models.TaskManagement;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ERP.Models
{
    public class ModelDbContext:DbContext
    {
        public ModelDbContext() : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }
     

        public DbSet<Issue> Issue { get; set; }
        public DbSet<IssueType> IssueType { get; set; }
        public DbSet<Attachment> Attachment { get; set; }
        public DbSet<IssueHistory> issueHistorie { get; set; }
        public DbSet<Comments> Comments { get; set; }
        public DbSet<EmpBasicInfo> EmpBasicInfo { get; set; }
        public DbSet<IssueWebLink> issueWebLink { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<Sprint> Sprint { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<ProjectUser> ProjectUser { get; set; }


        #region Instance

        private static ModelDbContext _db;
        public static ModelDbContext CreateInstance()
        {
            return _db ?? (_db = new ModelDbContext());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ModelDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
        #endregion
    }
}
