﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_Comments")]
    public class Comments 
    { 
        [Key]
        public int Id { get; set; }
        public int IssueId { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
       

    }
}
