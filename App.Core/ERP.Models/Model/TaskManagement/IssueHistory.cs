﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_IssueHistory")]
    public class IssueHistory : Entity
    {
        [Key]
        public int Id { get; set; }
        public int? ProjectId { get; set; }
        public int? SprintId { get; set; }
        public int? IssueId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }            
    }
}
