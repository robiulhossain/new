﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_Sprint")]
    public class Sprint
    {
        [Key]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string SprintTitle { get; set; }
        public string Discription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Completed { get; set; }
        public string Gole { get; set; }
        
       

    }
}
