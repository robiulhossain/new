﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_ProjectUser")]
    public class ProjectUser
    {
        [Key]
        public int Id { get; set; }
        public int? ProjectId { get; set; }
        public int? UserId { get; set; }
        

    }
}
