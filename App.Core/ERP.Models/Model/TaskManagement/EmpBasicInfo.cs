﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.Models.TaskManagement
{
    [Table("Jira_EmpBasicInfo")]
    public class EmpBasicInfo : Entity
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public int? Phone { get; set; }
        public string Email { get; set; }
        public string MaritalStatus { get; set; }
        public int? NID { get; set; }
        public DateTime JoiningDate { get; set; }
        public DateTime RegainDate { get; set; }
       
       

    }
}
