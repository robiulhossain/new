namespace ERP.Models.Constant
{
     public class Enums
    {
        public enum OEMSModule
        {
            STUDENT = 1,
            Attendence = 2,
            TaskManagement = 3,
            Fees = 4,
            SMS = 5,
            Routine = 6,
            LMS= 7,
            Accounts =8,
            Inventory= 9,
            HR_Payrol = 10,
            Library =  11,
                 User = 12,
            Employee = 13,
        }


        public enum OPERATOR
        {
            AIRTEL = 1,
            BANGLALINK = 2,
            GP = 3,
            ROBI = 4,
            TELETALK = 5
        }

        public enum GATEWAY
        {
            AIRTEL = 1,
            BANGLALINK = 2,
            GP = 3,
            ROBI = 4,
            TELETALK = 5,
            BOOMCAST = 6
        }


        public enum PRIORITY
        {
            High = 1,
            Medium = 2,
            Low = 3
        }
        public enum HttpStatus
        {
            
         Unauthorized        =  401,
         Forbidden           = 403,
         Not_Found           = 404,
         Method_Not_Allowed  = 405,
         Not_Acceptable      = 406,
         Precondition_Failed = 412,
         Internal_Server_Error = 500,
         Not_Implemented      = 501,
         Bad_Gateway          = 502,
         No_Data = 601

        }

        public const string NoImage = "~/assets/global/img/userimg.png";
        public enum ExamName
        {
            PSC ,
            JSC,
            SSC,
            HSC
        }
        public enum AddressType
        {
            present,
            permanent
        }
        public enum CommonDropDown
        {
            Gender,
            MaritalStatus,
            BloodGroup,
            Religion,
            Club,
            InterestIn,
            Organization,
            MemberofClub,
            Participation
        }
        public enum TCStatues
        {
            A, //Approved
            P  //pendding
        }
        public enum LeaveType
        {
            TC, //TransferCertificate
            TM,  //TestiMonial
            CC  //Character Certificate
        }

        public enum DayType
        {
            Regular,
            Holiday,
            Weekend
        }

        public enum LeaveStatus
        {
            Approved,
            Pending,
            Cancel
        }

        public enum FeesCategory
        {
            COMMON_FEES = 1,
            INDIVIDUL_FEES = 2,
            AUTOMATED_FEES = 3
        }

        public enum Coll_ReportName
        {
            Student_Summary,
            Student_Details,
            Student_Details_Indvidual,
            Users_Summary,
            Users_Summary_Group,
            Users_Details,
            Users_Details_Group,
            Headwise_Report,
            Head_Summary_Group,
            Modify_Report,
            Management_Report,
            Advance_Report
        }
        public enum Period
        {
            Yearly,
            Monthly,
            Date_Range,
            Daily
        }
        public enum SortBy
        {
          Roll_No=1,
          Posting_Date=2,
          Bank_Collection=3,
        }
        public enum PaymentType
        {
            Cash=1,
            Bank=2,
            Online=3
        }

        public enum IdConfigCode
        {
            AcademicCode,
            HouseCode,
            StudentType,
            ShiftCode,
            ClassCode,
            GroupCode,
            SectionCode,
            VersionCode,
            SessionCode,
            BranchCode
        }

        public enum FeesCheck
        {
            FP,//Fees Check
            FWP //Fees Without Check
        }
        public enum StudentStatus
        {
           I, //Inactive
           A,//Active
            D,//Delete
           WITHTC //With TC
        }
        public enum SubjectTypes
        {
            C,
            S,
            H,
            F,
            TH,
        }
        public enum ArchiveType
        {
            M,
            MT,
            MMP,
            MMF,
            G,
            GT,
            GMP,
            GMF,
            MainExamResult = 1,
            MainExamTab = 2,
            MainExamMarit = 3,
            MainExamMaint = 4,
            MainExamFail = 5,
            GrandExamResult = 6,
            GrandExamtab = 7,
            GrandExamMarit = 8,
            GrandExamFail = 9
        }
        public enum FeesPaymentStatus
        {
            Pending=1,
            Paid=2,
        }
        public enum SalaryHeadType
        {
            Earning = 1,
            Deduction = 2
        }
    }
}
